import key
import string

with open("ciphertext.txt", "r") as infile:
    ciphertext = infile.read()

# Concat everything into one big string, ignoring whitespace.
concatted_ciphertext = ''.join([c for c in ciphertext if c not in (' ', '\n')]).lower()


with open("letter_frequencies.dat", "r") as infile:
    raw = infile.readlines()
    split = [datum.strip().split('\t') for datum in raw]

    ENGLISH_LETTER_FREQUENCIES = [(datum[0], float(datum[1])) for datum in split]

def get_base_histogram():
    base = {}
    for c in string.ascii_lowercase:
        base[c] = 0
    return base


# key pos is 0 indexed
def get_histogram(key_pos, key_len, ciphertext):
    test = list(ciphertext)
    frequencies = get_base_histogram()
    for pos in range(key_pos, len(ciphertext), key_len):
        char = ciphertext[pos]
        frequencies[char] += 1
        test[pos] = " "

    return sorted(frequencies.items())

def find_potential_offsets(letter_frequencies, max_key_tries):

    frequencies = get_base_histogram()

    # Try all alphabetic shifts.
    for shift in range(0, 25 + 1):
        # normal_alphabet indexing.
        difference_sum = 0
        for current_index in range(0, 25 + 1):
            english_frequency = ENGLISH_LETTER_FREQUENCIES[current_index][1]

            # wrap around frequencie, 25 + 0 (start Z - A, A - B, )
            cipher_frequency = letter_frequencies[(shift + current_index) % 26][1]

            # Square the difference, so very large differences are more heavily penalized.
            difference_sum += int((english_frequency - cipher_frequency) ** 2)

        frequencies[string.ascii_lowercase[shift]] = difference_sum

    # Now find the smallest differences.
    # For each shift I have a difference sum.
    return sorted([(freq, letter) for letter, freq in frequencies.items()])[:max_key_tries]


def brute_force_attempts(key_offsets, ciphertext):
    pass


if __name__ == "__main__":
    # find repeated between 3-5 length
    WORD_RANGE = range(3, 6)
    TOP_N_FREQUENCIES = -10 # reverse list index
    MAX_KEY_TRIES = 5 # get top 4 likely candidate offsets per key.

    probable_key_size = key.find_probable_key_size(WORD_RANGE, concatted_ciphertext, 4, TOP_N_FREQUENCIES)
    # print(ENGLISH_LETTER_FREQUENCIES)

    key_offsets = []
    for key_pos in range(0, probable_key_size):
        letter_frequencies = get_histogram(key_pos, probable_key_size, concatted_ciphertext)
        potential_offsets = find_potential_offsets(letter_frequencies, MAX_KEY_TRIES)
        key_offsets.append(potential_offsets)

    print([offsets[0] for offsets in key_offsets])


    # get probable_key_size amount of distributions.
    # distribution is a map of letter to frequency.
    # get english language distribution.
    # compare using standard deviation, and get top results for that key.

    # using 6 * top results possibilities for each key, enumerate through all possible keys.
    # see which one looks like english
