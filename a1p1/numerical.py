import math
import itertools

def distance_factors(distance_map):
    distances = itertools.chain(*distance_map.values())

    # map of integer to list of its factors
    factors_cache = {}

    # map of a non-prime factor to how many times it occurs.
    factors_count = {}

    for distance in distances:
        if distance in factors_cache:
            factors = factors_cache[distance]

        else:
            factors = factorize(distance)
            factors_cache[distance] = factors

        for factor in factors:
            if factor not in factors_count:
                factors_count.setdefault(factor, 0)

            factors_count[factor] += 1

    return factors_count

def factorize(distance):
    if distance == 1:
        return [1]
    elif distance == 2:
        return [1,2]

    square_root_cutoff = math.ceil(distance ** 0.5) + 1
    bottom_factors = []
    for i in range(2, square_root_cutoff):
        if distance % i == 0:
            # print("distance {0} divisible by {1}".format(distance, i))
            bottom_factors.append(int(i))

    factors = bottom_factors[:]


    for bottom_factor in bottom_factors:
        top_factor = int(distance / bottom_factor)
        if top_factor != bottom_factor:
            factors.append(top_factor)

    return factors + [1, distance]
