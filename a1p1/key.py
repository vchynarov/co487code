import numerical

# Maps strings of word length in text to a list of their occuring indexes.
def find_frequencies(text, word_length):

    occurences = {}
    for i in range(word_length - 1, len(text)):
        current_word = text[i - word_length: i]
        occurences.setdefault(current_word, [])
        occurences[current_word].append(i)

    return occurences

def find_repeated(word_length, ciphertext):
    occurences = find_frequencies(ciphertext, word_length)
    # Only repeated elements.
    return dict((filter(lambda item: len(item[1]) > 1, occurences.items())))

def construct_frequency_map(word_range, ciphertext):
    merged_frequencies = {}
    for wr in word_range:
        merged_frequencies.update(find_repeated(wr, ciphertext))

    return merged_frequencies

def construct_distance_map(frequency_map):
    distance_map = {}

    for repeated_word in frequency_map:
        distances = []
        positions = frequency_map[repeated_word]

        # Start enumerating from second pos, and get previous position
        for i, pos in list(enumerate(positions))[1:]:
            prev_pos = positions[i - 1]
            distances.append(pos - prev_pos)

        distance_map[repeated_word] = distances

    return distance_map

def find_probable_key_size(word_range, ciphertext, min_key_size, top_frequencies):
    frequency_map = construct_frequency_map(word_range, ciphertext)
    # print(frequency_map)

    distance_map = construct_distance_map(frequency_map)
    # print(distance_map)

    factors = numerical.distance_factors(distance_map)

    # sort distances, find top 10 things
    maxed = sorted([(freq, item) for (item, freq) in factors.items()])[top_frequencies:]

    # Order in descending order, only care about the first one factor greater than min_key_size
    for frequency, factor in reversed(maxed):
        if factor >= min_key_size:
            return factor
