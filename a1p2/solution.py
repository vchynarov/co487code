
import string

def is_word(word):
    for c in word:
        if not c.isalpha():
            return False
    return True

with open("words.txt", "r") as infile:
    # assumption
    words = [word.strip().upper() for word in infile.readlines()]

# Assume first character is a space, skip it in C1.
C1 = "OAZRNWPLLRDLJ"
C2 = "JAYUNWUM WHYB"

D = "E AX  VZLVWNH"


# Space is 26th character.
LETTER_MAP = {' ': 0}
    
# Start counting - 1.
REVERSE_MAP = ' ' + string.ascii_uppercase

for i, c in enumerate(string.ascii_uppercase):
    LETTER_MAP[c] = i + 1

def subtract_letter(start, subtraction):
    # modulo 27 since 27 letters.
    return REVERSE_MAP[(LETTER_MAP[start] - LETTER_MAP[subtraction]) % 27]

if __name__ == "__main__":
    correct_words = [word for word in words if is_word(word)] 

    for word in correct_words:
        c1_chunk = C1[:len(word)]
        c2_chunk = C2[:len(word)]
        key_attempt = ''.join(subtract_letter(pair[0], pair[1]) for pair in zip(c1_chunk, word))
        m2_attempt = ''.join(subtract_letter(pair[0], pair[1]) for pair in zip(c2_chunk, key_attempt))
        print("{0} \t key: {1}, word: {2}".format(m2_attempt, key_attempt, word))
